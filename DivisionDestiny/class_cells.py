import math
import numpy.random as nprand

class MyTCell:
    """This is a class to create a T cell that moves inside the grid"""
    def __init__(self,du,fac,Num):
        self.ID = 'CD8_' + str(Num)     #ID/Name
        self.gen = 0        #Generations
        self.birth = 0      #Time of birth
        self.mother = 'NA'  #Parent cell
        self.eve = 'NA'     #Family
        self.pm = 0.0       #To determine the division time
        self.pgain = 0.0    #Accumulation of rm
        self.div = 0        #Time of division
        self.gdiv = 0       #Global time of division
        self.tm = self.LScale(du,fac*du)   #Death time of mother
        self.tk = self.Nemesis(0.99,0.05*0.99)     #Death time
        self.die = 0        #Time of death
        self.gdie = 0       #Global time of death
        self.state = "A"    #A=Alive, T=Terminate, D=Dead
        self.CD62L = 0.0    #A marker that decreases over time
        self.KLRG1 = 0.0    #A marker that decreases over time
        self.CCR7 = 0.0     #A marker that can decrease or increase
        self.firstdiv = self.Genesis(0.0,0.6,0.0,200.0,48.0) #The first division
        self.MD = 20
    
    def Genesis(self,mu,sd,mn,mx,fac=1.0):
        s = 2*mx
        #a = mu/math.sqrt(1+sd/(mu*mu))
        #b = math.log(1+sd/(mu*mu))
        while (s < 0.0 or s > int(mx)):
            s = nprand.lognormal(mu,sd)*fac
        return s
        
    def Scaler(self,mu,sd,mn,mx):
        s = 2*mx
        while (s < int(mn) or s > int(mx)):
            s = nprand.normal(mu,sd)
        return s

    def tdiv(self,prm,prg,tmin,prv,tp):
        self.div = tmin + prm*prg*prv
        self.gdiv = self.div + int(tp)

    def LScale(self,u,s,m=10,n=0.0):
        mx = m*u; x = -1
        mu = math.log(u/math.sqrt(1.0+s/(u*u)))
        sd = math.sqrt(math.log(1.0+s/(u*u)))
        while (x < n or x > int(mx)):
            x = nprand.lognormal(mu,sd)
        return x

    def LDestiny(self,u,s,mx=20,mn=1):
        x=0
        mu = math.log(u/math.sqrt(1.0+s/(u*u)))
        sd = math.sqrt(math.log(1.0+s/(u*u)))
        while (x < mn or x > mx):
            x = nprand.lognormal(mu,sd)
        #print mu, sd, x
        return int(x)
    
    def Nemesis(self,u,sd):
        mx = 2*u; s = -1;
        while (s < 0.0 or s > int(mx)):
            s = nprand.normal(u,sd)
        return s
        
    def tdeath(self,tdm,kd,tp):
        self.die = tdm*kd
        self.gdie = self.die + int(tp)
