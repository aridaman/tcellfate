# -*- coding: utf-8 -*-
"""
Created on Sun Apr 05 10:56:59 2015
To extract temporal data
@author: Aridaman
2 index of list is Birth,
4 index of list is Global TDivide,
6 index of list is Global TDie,
7 index of list is CD62L,
8 index of list is CCR7,
9 index of list is KLRG1,
"""
import csv, matplotlib, argparse
import matplotlib.pylab as plt
from matplotlib import rcParams
matplotlib.use('Agg')
import numpy as np

data = {}
c62_t=97; cr7_t=90; kg1_t=80

parser = argparse.ArgumentParser()
parser.add_argument('-fn', dest='fn', default="BP_CD8_4.5_1.25_0.2_1000_20_10.csv", type=str, help='File Name')
parser.add_argument('-Fam', dest='Fam', default=1000, type=int, help='Number of families')
val = parser.parse_args()

with open(val.fn,'r') as fulldata:   #Read from file
    lines = csv.reader(fulldata,delimiter=',',skipinitialspace=True)
    next(lines)
    for ele in lines:
        if ele[0] not in data:
            data[ele[0]] = {}
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        elif (ele[0] in data) and (ele[1] not in data[ele[0]]):
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        else:
            data[ele[0]][ele[1]] += [ele[2::]]

def marker_data(data, Fam=0,c62_t=97,cr7_t=90,kg1_t=80,day=8):
    time = 24.0*day
    res = {'cd62':[], 'cr7':[], 'kg1':[], 'r_cm':[], 'r_em':[], 'r_f':[], 'r_tot':[]} # Create an empty dictionary to store normalized marker values
    for i in range(Fam):
        fam = str(i)
        tot = 0; #c62p = 0; cr7p = 0; kg1p = 0;
        for gen in data[fam]:
            for cell in range(len(data[fam][gen])):
                #print data[fam][gen][cell]
                if (float(data[fam][gen][cell][6]) > time) and (float(data[fam][gen][cell][4]) > time) and (float(data[fam][gen][cell][2]) < time):
                    tot += 1
                    res['cd62'] += [float(data[fam][gen][cell][7])/c62_t] # Normalize wrt threshold
                    res['cr7'] += [float(data[fam][gen][cell][8])/cr7_t] # Normalize wrt threshold
                    res['kg1'] += [float(data[fam][gen][cell][9])/kg1_t] # Normalize wrt threshold
                    res['r_tot'] += [24/float(data[fam][gen][cell][3])] # Calculate rate
                    if (float(data[fam][gen][cell][7])/c62_t >= 1) & (float(data[fam][gen][cell][8])/cr7_t >= 1):
                        res['r_cm'] += [24/float(data[fam][gen][cell][3])] # Calculate rate
                    elif (float(data[fam][gen][cell][7])/c62_t < 1) & (float(data[fam][gen][cell][8])/cr7_t >= 1):
                        res['r_em'] += [24/float(data[fam][gen][cell][3])] # Calculate rate                       
                    elif (float(data[fam][gen][cell][7])/c62_t < 1) & (float(data[fam][gen][cell][8])/cr7_t < 1):
                        res['r_f'] += [24/float(data[fam][gen][cell][3])] # Calculate rate
    return res

bp_data={} # initialize the marker/day object

for t in range(1,9): # To get all marker data per day
    bp_data["day "+str(t)] = marker_data(data,Fam=val.Fam,day=t)
    print len(bp_data)


## Plot Proliferation Rate
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['r_cm'],bp_data['day 2']['r_cm'],bp_data['day 3']['r_cm'],bp_data['day 4']['r_cm'],bp_data['day 5']['r_cm'],bp_data['day 6']['r_cm'],bp_data['day 7']['r_cm'],bp_data['day 8']['r_cm']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Proliferation_cm_daydist.svg',format='svg')


## Plot Proliferation Rate
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['r_em'],bp_data['day 2']['r_em'],bp_data['day 3']['r_em'],bp_data['day 4']['r_em'],bp_data['day 5']['r_em'],bp_data['day 6']['r_em'],bp_data['day 7']['r_em'],bp_data['day 8']['r_em']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Proliferation_em_daydist.svg',format='svg')


## Plot Proliferation Rate
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['r_f'],bp_data['day 2']['r_f'],bp_data['day 3']['r_f'],bp_data['day 4']['r_f'],bp_data['day 5']['r_f'],bp_data['day 6']['r_f'],bp_data['day 7']['r_f'],bp_data['day 8']['r_f']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Proliferation_f_daydist.svg',format='svg')


## Plot Proliferation Rate
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['r_tot'],bp_data['day 2']['r_tot'],bp_data['day 3']['r_tot'],bp_data['day 4']['r_tot'],bp_data['day 5']['r_tot'],bp_data['day 6']['r_tot'],bp_data['day 7']['r_tot'],bp_data['day 8']['r_tot']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Proliferation_tot_daydist.svg',format='svg')


## Plot Proliferation Rate
import pandas as pd
import seaborn as sns
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_cm = [bp_data['day 1']['r_cm'],bp_data['day 2']['r_cm'],bp_data['day 3']['r_cm'],bp_data['day 4']['r_cm'],bp_data['day 5']['r_cm'],bp_data['day 6']['r_cm'],bp_data['day 7']['r_cm'],bp_data['day 8']['r_cm']]
plot_em = [bp_data['day 1']['r_em'],bp_data['day 2']['r_em'],bp_data['day 3']['r_em'],bp_data['day 4']['r_em'],bp_data['day 5']['r_em'],bp_data['day 6']['r_em'],bp_data['day 7']['r_em'],bp_data['day 8']['r_em']]
plot_f = [bp_data['day 1']['r_f'],bp_data['day 2']['r_f'],bp_data['day 3']['r_f'],bp_data['day 4']['r_f'],bp_data['day 5']['r_f'],bp_data['day 6']['r_f'],bp_data['day 7']['r_f'],bp_data['day 8']['r_f']]

temp_day = []; temp_exp = []; temp_marker = []
for i in range(len(plot_cm)):
    for j in range(len(plot_cm[i])):
        temp_day += [i+1]; temp_exp += [plot_cm[i][j]]; temp_marker += ['cm']
    for j in range(len(plot_em[i])):
        temp_day += [i+1]; temp_exp += [plot_em[i][j]]; temp_marker += ['em']
    for j in range(len(plot_f[i])):
        temp_day += [i+1]; temp_exp += [plot_f[i][j]]; temp_marker += ['f']

df = pd.DataFrame(dict(day = temp_day, exp= temp_exp, marker = temp_marker))
sns.violinplot(x="day", y="exp", hue="marker", data=df, palette = {'cm': 'red', 'em': 'gray', 'f': 'blue'})
plt.savefig('Proliferation_day1-8_daydist.svg',format='svg')

plt.figure()
sns.violinplot(x="marker", y="exp", data=df, palette = {'cm': 'red', 'em': 'gray', 'f': 'blue'})
plt.savefig('Proliferation_marker_day1-8_daydist.svg',format='svg')

temp_day = []; temp_exp = []; temp_marker = []
for i in range(4,len(plot_cm)):
    for j in range(len(plot_cm[i])):
        temp_day += [i+1]; temp_exp += [plot_cm[i][j]]; temp_marker += ['cm']
    for j in range(len(plot_em[i])):
        temp_day += [i+1]; temp_exp += [plot_em[i][j]]; temp_marker += ['em']
    for j in range(len(plot_f[i])):
        temp_day += [i+1]; temp_exp += [plot_f[i][j]]; temp_marker += ['f']

df = pd.DataFrame(dict(day = temp_day, exp= temp_exp, marker = temp_marker))
plt.figure()
sns.violinplot(x="day", y="exp", hue="marker", data=df, palette = {'cm': 'red', 'em': 'gray', 'f': 'blue'})
plt.savefig('Proliferation_day5-8_daydist.svg',format='svg')

plt.figure()
sns.violinplot(x="marker", y="exp", data=df, palette = {'cm': 'red', 'em': 'gray', 'f': 'blue'})
plt.savefig('Proliferation_marker_day5-8_daydist.svg',format='svg')

############ Plotting the marker distribution per generation ############
def marker_generation(data, Fam=0,c62_t=97,cr7_t=90,kg1_t=80, gen=1, day=1):
    #time = 24.0*day
    gen = str(gen)
    print gen, Fam#, time
    res = {'cd62':[], 'cr7':[], 'kg1':[], 'r_cm':[], 'r_em':[], 'r_f':[]} # Create an empty dictionary to store normalized marker values
    for i in range(Fam):
        fam = str(i)
        tot = 0; #c62p = 0; cr7p = 0; kg1p = 0;
        #print len(data[fam][gen])
        try:
            for cell in range(len(data[fam][gen])):
                #print len(data[fam][gen][cell])
                #if (float(data[fam][gen][cell][6]) > time) and (float(data[fam][gen][cell][4]) > time) and (float(data[fam][gen][cell][2]) < time):
                tot += 1
                res['cd62'] += [float(data[fam][gen][cell][7])/c62_t] # Normalize wrt threshold
                res['cr7'] += [float(data[fam][gen][cell][8])/cr7_t] # Normalize wrt threshold
                res['kg1'] += [float(data[fam][gen][cell][9])/kg1_t] # Normalize wrt threshold'''
                if (float(data[fam][gen][cell][7])/c62_t >= 1) & (float(data[fam][gen][cell][8])/cr7_t >= 1):
                    res['r_cm'] += [24/float(data[fam][gen][cell][3])] # Calculate rate
                elif (float(data[fam][gen][cell][7])/c62_t < 1) & (float(data[fam][gen][cell][8])/cr7_t >= 1):
                    res['r_em'] += [24/float(data[fam][gen][cell][3])] # Calculate rate                       
                elif (float(data[fam][gen][cell][7])/c62_t < 1) & (float(data[fam][gen][cell][8])/cr7_t < 1):
                    res['r_f'] += [24/float(data[fam][gen][cell][3])] # Calculate rate
        except:
            print "Does not exist!"            
    return res

gen_cd62=[]; gen_cr7=[]; gen_kg1=[]
gen_rcm=[]; gen_rem=[]; gen_rf=[]

for gen in range(1,17): # To get all marker data per day
    print "gen = ", gen
    temp = marker_generation(data, Fam=val.Fam,day=2,gen=gen)
    #print temp
    gen_cd62 += [temp['cd62']]; gen_cr7 += [temp['cr7']]; gen_kg1 += [temp['kg1']]
    gen_rcm += [temp['r_cm']]; gen_rem += [temp['r_em']]; gen_rf += [temp['r_f']]


temp_day = []; temp_exp = []; temp_marker = []
for i in range(0,len(gen_rcm)):
    for j in range(len(gen_rcm[i])):
        temp_day += [i+1]; temp_exp += [gen_rcm[i][j]]; temp_marker += ['cm']
    for j in range(len(gen_rem[i])):
        temp_day += [i+1]; temp_exp += [gen_rem[i][j]]; temp_marker += ['em']
    for j in range(len(gen_rf[i])):
        temp_day += [i+1]; temp_exp += [gen_rf[i][j]]; temp_marker += ['f']

df = pd.DataFrame(dict(generation = temp_day, exp= temp_exp, marker = temp_marker))
plt.figure()
#sns.set(rc={'figure.figsize':(8,6)})
sns.violinplot(x="generation", y="exp", hue="marker", data=df, palette = {'cm': 'red', 'em': 'gray', 'f': 'blue'})
plt.savefig('Proliferation_rate_generation_daydist.svg',format='svg')

plt.figure()
sns.violinplot(x="generation", y="exp", data=df, color="gray")
plt.savefig('Proliferation_total_generation_dist.svg',format='svg')
