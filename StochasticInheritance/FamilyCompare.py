# -*- coding: utf-8 -*-
"""
Created on Sun Apr 05 10:56:59 2015
To extract temporal data
@author: Aridaman
2 index of list is Birth,
4 index of list is Global TDivide,
6 index of list is Global TDie,
7 index of list is CD62L,
8 index of list is CCR7,
9 index of list is KLRG1,
"""
import csv, argparse
from numpy import std, mean
import matplotlib.pylab as plt
from matplotlib import rcParams

parser = argparse.ArgumentParser()
parser.add_argument('-fn', dest='fn', default="BP_CD8_4.5_1.25_0.2_1000_20_10.csv", type=str, help='File Name')
parser.add_argument('-Fam', dest='Fam', default=1000, type=int, help='Number of families')
val = parser.parse_args()

data = {}
with open(val.fn,'r') as fulldata:   #Read from file
    lines = csv.reader(fulldata,delimiter=',',skipinitialspace=True)
    next(lines)
    for ele in lines:
        if ele[0] not in data:
            data[ele[0]] = {}
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        elif (ele[0] in data) and (ele[1] not in data[ele[0]]):
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        else:
            data[ele[0]][ele[1]] += [ele[2::]]

def days_data(data, Fam=0):
    res = {}
    for i in range(Fam):
        fam = str(i)
        res[i] = {}
        for gen in data[fam]:
            tot = 0
            tdiv = []; tdie = []
            for cell in range(len(data[fam][gen])):
                tot += 1
                tdiv += [float(data[fam][gen][cell][3])]
                tdie += [float(data[fam][gen][cell][5])]
            res[i][gen] = [mean(tdiv),mean(tdie),tot,tot/mean(tdiv)]
    return res

Rates = days_data(data,Fam=val.Fam)

rcParams.update({'figure.autolayout': True})
Tv = {}
Td = {}
Tot = {}
for i in Rates.keys():
    for gen in Rates[i]:
        if gen not in Tv:
            Tv[gen] = [Rates[i][gen][0]]
            Td[gen] = [Rates[i][gen][1]]
            Tot[gen] = [Rates[i][gen][3]]
        else:
            Tv[gen] += [Rates[i][gen][0]]
            Td[gen] += [Rates[i][gen][1]]
            Tot[gen] += [Rates[i][gen][3]]

plt.figure()
test = plt.violinplot(Tv.values(),[int(i) for i in Tv.keys()],showmeans=True,showextrema=False,widths=0.6)
for pc in test['bodies']: pc.set_color('black')
plt.axis((-0.5, 16.5, 0, 50))
plt.yticks([0,10,20,30,40,50])
plt.xlabel('Generation',fontsize=20)
plt.ylabel('Time',fontsize=20)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.savefig('Div_Rates.png',format='png',dpi=300)

plt.figure()
test = plt.violinplot(Td.values(),[int(i) for i in Td.keys()],showmeans=True,showextrema=False,widths=0.6)
for pc in test['bodies']: pc.set_color('black')
plt.axis((-0.5, 16.5, 0, 50))
plt.yticks([0,10,20,30,40,50])
plt.xlabel('Generation',fontsize=20)
plt.ylabel('Time',fontsize=20)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.savefig('Death_Rates.png',format='png',dpi=300)
