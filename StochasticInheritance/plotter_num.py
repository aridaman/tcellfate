#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 12:18:49 2015

@author: Aridaman
"""
import csv, argparse, matplotlib
matplotlib.use('Agg')
from numpy import mean, logspace
from math import ceil
import matplotlib.pylab as plt
import numpy as np
import scipy.stats.stats as stat
from matplotlib import rcParams

def roundup(val, fac=100000):
    return int(ceil(val*1.0 / fac)) * fac

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx

def plotter(Fam=0, Thresh=[75,90,35]):
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', dest='NCD62', default='Fac_CD62L_4.5_1.25_0.2_1000_20_10.csv', help='Name of CD62L file')
    parser.add_argument('-r', dest='NCCR7', default='Fac_CCR7_4.5_1.25_0.2_1000_20_10.csv', help='Name of CCR7 file')
    parser.add_argument('-k', dest='NKLRG', default='Fac_KLRG1_4.5_1.25_0.2_1000_20_10.csv', help='Name of KLRG1 file')
    arg = parser.parse_args()
    cd62l_ite = []  #To store cd62l values for all iterations
    ccr7_ite = []   #To store ccr7 values for all iterations
    klrg1_ite = []  #To store klrg1 values for all iterations
    c62pos = []     #To count cells above cd62l threshold
    cc7pos = []     #To count cells above ccr7 threshold
    kg1pos = []     #To count cells above klrg1 threshold
    f_c62pos = []     #To count cells above cd62l threshold
    f_cc7pos = []     #To count cells above ccr7 threshold
    f_kg1pos = []     #To count cells above klrg1 threshold
    famlens= []     #lengths of each family
    total_cells = 0 #To get total number of cells produced
    with open(arg.NCD62,'r') as fcd62l:   #Read from file
        lines = csv.reader(fcd62l,delimiter=' ')
        for ele in lines:
            cd62l_ite += [ele[:]]
    with open(arg.NCCR7,'r') as fccr7:     #Read from file
        lines = csv.reader(fccr7,delimiter=' ')
        for ele in lines:
            ccr7_ite += [ele[:]]
    with open(arg.NKLRG,'r') as fklrg1:     #Read from file
        lines = csv.reader(fklrg1,delimiter=' ')
        for ele in lines:
            klrg1_ite += [ele[:]]
    ii=-1
    for i in range(len(cd62l_ite)):
        if len(cd62l_ite[i]) > 0:
            ii+=1
            famlens += [len(cd62l_ite[i])]
            total_cells += len(cd62l_ite[i])
            c62pos += [sum(float(val) > Thresh[0] for val in cd62l_ite[i])]
            cc7pos += [sum(float(val) > Thresh[1] for val in ccr7_ite[i])]
            kg1pos += [sum(float(val) > Thresh[2] for val in klrg1_ite[i])]
            
            f_c62pos += [sum(float(val) > Thresh[0] for val in cd62l_ite[i])*100.0/len(cd62l_ite[i])]
            f_cc7pos += [sum(float(val) > Thresh[1] for val in ccr7_ite[i])*100.0/len(cd62l_ite[i])]
            f_kg1pos += [sum(float(val) > Thresh[2] for val in klrg1_ite[i])*100.0/len(cd62l_ite[i])]
            #print "Family ", i, ": ", ceil(c62pos[i]), ceil(cc7pos[i]), ceil(kg1pos[i]), famlens[i]
            cd = [float(val) for val in cd62l_ite[i]]
            cc = [float(val) for val in ccr7_ite[i]]
            kl = [float(val) for val in klrg1_ite[i]]
            print "Family ", i, ": ", mean(cd), mean(cc), mean(kl), famlens[ii]
    print "Total number of cells: ", total_cells
    
    rcParams.update({'figure.autolayout': True})
    
    ###PLOT CUMSUM
    rcParams.update({'figure.autolayout': True})
    plt.figure()
    sorted_data = np.sort(famlens)
    data_cumsum = np.cumsum(sorted_data[::-1])
    axis_max = roundup(data_cumsum[-1],100000)
    plt.axis((0,100,0,axis_max))
    labels = [str(axis_max/4), str(axis_max/2), str(3*axis_max/4), str(axis_max)]
    plt.step(np.arange(sorted_data.size)*100.0/np.arange(sorted_data.size)[-1], data_cumsum,color='k',linewidth=2)
    print 100.0*find_nearest(data_cumsum,data_cumsum[-1]/2)[1]/np.arange(sorted_data.size)[-1]
    sum_half = 100.0*find_nearest(data_cumsum,np.cumsum(sorted_data[::-1])[-1]/2)[1]/np.arange(sorted_data.size)[-1]
    plt.yticks([axis_max/4,axis_max/2,3*axis_max/4,axis_max], labels)
    plt.plot([0,sum_half],[total_cells/2.0,total_cells/2.0],linestyle='--',color='r')
    plt.plot([sum_half,sum_half],[0,total_cells/2.0],linestyle='--',color='r')
    plt.xlabel('Percentage of cells (%)', fontsize=20)
    plt.ylabel('CD8+ T cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Hist_CumSum.svg',format='svg',dpi=300)
    
    ## Plot marker distribution vs family size
    plt.figure()
    plt.hist(famlens, bins=logspace(0.1, 5.0, 20),color='r')
    plt.xscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('Frequency', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Hist_Family.svg',format='svg',dpi=300)
    plt.figure()
    
    plt.axis((1,axis_max,0,100))
    plt.plot(famlens,f_c62pos,linestyle='',marker='o',color='k',label='CD62L')
    plt.xscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('% of CD62L+ cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Hist_CD62L.svg', format='svg',dpi=300)
    
    plt.figure()
    plt.axis((1,axis_max,0,100))
    plt.plot(famlens,f_cc7pos,linestyle='',marker='o',color='k',label='CCR7')
    plt.xscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('% of CD27+ cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Hist_CD27.svg', format='svg',dpi=300)
    
    plt.figure()
    plt.axis((1,axis_max,0,100))
    plt.plot(famlens,f_kg1pos,linestyle='',marker='o',color='k',label='KLRG1')
    plt.xscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('% of KLRG1+ cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Hist_KLRG1.svg', format='svg',dpi=300)
    
    plt.figure()
    plt.plot(famlens,c62pos,linestyle='',marker='o',color='k',label='CD62L')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('CD62L+ cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Num_CD62L.svg', format='svg')
    
    plt.figure()
    plt.plot(famlens,cc7pos,linestyle='',marker='o',color='k',label='CCR7')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('CD27+ cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Num_CD27.svg', format='svg')
    plt.figure()

    plt.plot(famlens,kg1pos,linestyle='',marker='o',color='k',label='KLRG1')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Family size', fontsize=20)
    plt.ylabel('KLRG1+ cells', fontsize=20)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.savefig('Num_KLRG1.svg', format='svg')
    
    print "CD62L corr: ", stat.spearmanr(famlens,c62pos)
    print "CCR7 corr: ", stat.spearmanr(famlens,cc7pos)
    print "KLRG1 corr: ", stat.spearmanr(famlens,kg1pos)

plotter(Fam=1)
