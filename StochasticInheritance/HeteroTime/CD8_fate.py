#!/usr/bin/python
from class_cells import MyTCell
import csv, argparse #, itertools
#import numpy as np
#from progress.bar import Bar
from numpy import median

def grow_62L(A,B):
    x = 0.5*A.CD62L*B.Scaler(1.9, 0.1, 0.0, 3.7)
    if x>200.0:
        x = 200.0
    elif x<0.0:
        x = 0.0
    return x
def grow_R7(A,B):
    x = 0.5*A.CCR7*B.Scaler(2.0, 0.3, 0.0, 4.0)
    if x>200.0:
        x = 200.0
    elif x<0.0: 
        x = 0.0
    return x
def grow_G1(A,B):
    x = 0.5*A.KLRG1*B.Scaler(2.3, 0.3, 0.0, 4.6)
    if x>200.0:
        x = 200.0
    elif x<0.0: 
        x = 0.0
    return x

parser = argparse.ArgumentParser()
parser.add_argument('-i', dest='Cells', default=200, type=int, help='Number of Cells')
parser.add_argument('-p', dest='pm', default=4.5, type=float, help='Mean tdiv for mothers')
parser.add_argument('-m', dest='mg', default=1.25, type=float, help='Mean of gain')
parser.add_argument('-f', dest='fac', default=0.2, type=float, help='Factor for all SD')
parser.add_argument('-MD', dest='div', default=20, type=int, help='Max divisions')
val = parser.parse_args()
Name = str(val.pm)+'_'+str(val.mg)+'_'+str(val.fac)+'_'+str(val.Cells)+'_'+str(val.div)

#Parameters
N=1		#Total number of cells
T=192.0		#Total times
fac=val.fac	#Factor to scale the sigmas
pm=val.pm	#mu_mother (-1.0 for lognormal)
ps=fac*pm	#sigma_mother (1.0 for lognormal)
gm=val.mg	#mu_gain
gs=0.05*gm	#sigma_gain
epm=1.0
eps=0.02
du=40.0		#mu_death
MDiv=val.div	#Maximum divisions possible
tmin=4.0	#minimum time to divide

# Other factors
m_cd62l = 100.0
m_ccr7 = 100.0
m_klrg1 = 10.0

fref = open('Freq_'+Name+'.csv', 'w')
writer=csv.writer(fref,delimiter=' ',quoting=csv.QUOTE_MINIMAL)
fac62 = open('Fac_CD62L_'+Name+'.csv', 'w')
fac_62=csv.writer(fac62,delimiter=' ',quoting=csv.QUOTE_MINIMAL)
fac7 = open('Fac_CCR7_'+Name+'.csv', 'w')
fac_7=csv.writer(fac7,delimiter=' ',quoting=csv.QUOTE_MINIMAL)
facG1 = open('Fac_KLRG1_'+Name+'.csv', 'w')
fac_G1=csv.writer(facG1,delimiter=' ',quoting=csv.QUOTE_MINIMAL)
foo = open('BP_CD8_'+Name+'.csv', 'w')
foo.write("Ite, Gen, ID, Mother, Birth, Divide, GloDiv, Die, GloDie, CD62L, CCR7, KLRG1, rm, rgain, tm, tk, State\n")

Res = []

for ite in range(val.Cells):
    #print "\nmm:",mm,"sm:",sm,"mg:",mg,"sg:",sg,"mv:",mv,"sv:", sv, "du:", du, "ite:", ite+1
    #Define the variables
    cd8 = [None]*N
    Freq = [0]*(N+1)
    Factors = {}
    for i in range(N):
        cd8[i] = MyTCell(du,fac,i)
        cd8[i].pm = cd8[i].LScale(pm,ps) 	#Assign resource for the undivided cell
        cd8[i].pgain = cd8[i].LScale(gm,gs) 	#Assign gain of resource
        cd8[i].tdiv(cd8[i].pm,cd8[i].pgain,tmin,1.0,cd8[i].firstdiv)	#Assign division time
        cd8[i].die = cd8[i].tm
        cd8[i].gdie = cd8[i].tm+cd8[i].firstdiv #Assign time of death
        cd8[i].gen = 0
        cd8[i].eve = cd8[i].ID
        cd8[i].CD62L = cd8[i].Scaler(m_cd62l,0.01*m_cd62l,0.0,1.25*m_cd62l)
        cd8[i].CCR7 = cd8[i].Scaler(m_ccr7,0.01*m_ccr7,0.0,1.25*m_ccr7)
        cd8[i].KLRG1 = cd8[i].Scaler(m_klrg1,0.01*m_klrg1,0.0,1.25*m_klrg1)

    for g in range(1,MDiv+1):
        for i in range(len(cd8)):
            if (cd8[i].gen==g-1):
                if (cd8[i].state=="A" and min(cd8[i].gdie,cd8[i].gdiv)<T):
                    #1st daughter from mother cells
                    cd8.extend([MyTCell(du,fac,len(cd8))]) #Create new daughter
                    cd8[-1].mother = cd8[i].ID; cd8[-1].eve=cd8[i].eve; cd8[-1].gen=g #Parent, Family and generation
                    cd8[-1].pm = cd8[i].pm*cd8[i].pgain*cd8[-1].Scaler(epm,eps,0.5,1.5) 	#Tdiv for the cell
                    cd8[-1].pgain = cd8[-1].LScale(gm,gs) 	#Gain of tdiv
                    cd8[-1].tm = cd8[i].die			#Daughter inherits the death time
                    cd8[-1].tdiv(cd8[-1].pm,cd8[-1].pgain,tmin,1.0,cd8[i].gdiv) #Assign time of division
                    cd8[-1].birth = cd8[i].gdiv
                    cd8[-1].tdeath(cd8[-1].tm,cd8[-1].tk,cd8[i].gdiv) #Assign time of birth and potential death
                    cd8[-1].CD62L = grow_62L(cd8[i],cd8[-1])
                    cd8[-1].CCR7 = grow_R7(cd8[i],cd8[-1])
                    cd8[-1].KLRG1 = grow_G1(cd8[i],cd8[-1])
                    if (cd8[-1].gdiv>cd8[-1].gdie and cd8[-1].gdie<=T): cd8[-1].state="T"
                
                    #2nd daughter from mother cells
                    cd8.extend([MyTCell(du,fac,len(cd8))]) #Create new daughter
                    cd8[-1].mother = cd8[i].ID; cd8[-1].eve=cd8[i].eve; cd8[-1].gen=g #Parent, Family and generation
                    cd8[-1].pm = cd8[i].pm*cd8[i].pgain*cd8[-1].Scaler(epm,eps,0.5,1.5) 	#Resource for the cell
                    cd8[-1].pgain = cd8[-1].LScale(gm,gs) 	#Gain of resource
                    cd8[-1].tm = cd8[i].die #Daughter inherits the death time
                    cd8[-1].tdiv(cd8[-1].pm,cd8[-1].pgain,tmin,1.0,cd8[i].gdiv) #Assign time of division
                    cd8[-1].birth = cd8[i].gdiv
                    cd8[-1].tdeath(cd8[-1].tm,cd8[-1].tk,cd8[i].gdiv) #Assign time of birth and potential death
                    cd8[-1].CD62L = grow_62L(cd8[i],cd8[-1])
                    cd8[-1].CCR7 = grow_R7(cd8[i],cd8[-1])
                    cd8[-1].KLRG1 = grow_G1(cd8[i],cd8[-1])
                    if (cd8[-1].gdiv>cd8[-1].gdie and cd8[-1].gdie<=T): cd8[-1].state="T" #Says that the cells die
                    cd8[i].state="D"
                    
    Factors['CD62L'] = []
    Factors['CCR7'] = []
    Factors['KLRG1'] = []
    for i in range(len(cd8)):
        for j in range(N):
            if (cd8[i].eve=='CD8_'+str(j)) and (cd8[i].state=="A"):
                Freq[j+1] = Freq[j+1] + 1
                Factors['CD62L'] += [cd8[i].CD62L]
                Factors['CCR7'] += [cd8[i].CCR7]
                Factors['KLRG1'] += [cd8[i].KLRG1]

    print "Iteration: ", ite, "Total alive: ", sum(Freq)#, "Total cells: ", len(cd8), cd8[0].firstdiv
    #Res += [{'Alive':sum(Freq), 'Total':len(cd8)}]
    Freq[0] = ite
    Factors[0] = ite
    writer.writerow(Freq)
    fac_62.writerow(Factors['CD62L'])
    fac_7.writerow(Factors['CCR7'])
    fac_G1.writerow(Factors['KLRG1'])
    for i in range(len(cd8)):
	    out=str([ite, cd8[i].gen, cd8[i].ID, cd8[i].mother, cd8[i].birth, cd8[i].div, cd8[i].gdiv, cd8[i].die, cd8[i].gdie, cd8[i].CD62L, cd8[i].CCR7, cd8[i].KLRG1, cd8[i].pm, cd8[i].pgain, cd8[i].tm, cd8[i].tk, cd8[i].state]).strip('[]')
	    foo.write(out+"\n")

#print max([Res[i]['Alive'] for i in range(len(Res))]), median([Res[i]['Alive'] for i in range(len(Res))])
