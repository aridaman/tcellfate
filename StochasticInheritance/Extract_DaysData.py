# -*- coding: utf-8 -*-
"""
Created on Sun Apr 05 10:56:59 2015
To extract temporal data
@author: Aridaman
2 index of list is Birth,
4 index of list is Global TDivide,
6 index of list is Global TDie,
7 index of list is CD62L,
8 index of list is CCR7,
9 index of list is KLRG1,
"""
import csv, matplotlib, argparse
matplotlib.use('Agg')
from numpy import std, mean
import matplotlib.pylab as plt
from matplotlib import rcParams

parser = argparse.ArgumentParser()
parser.add_argument('-fn', dest='fn', default="BP_CD8_4.5_1.25_0.2_1000_20_10.csv", type=str, help='File Name')
parser.add_argument('-Fam', dest='Fam', default=1000, type=int, help='Number of families')
val = parser.parse_args()

data = {}
with open(val.fn,'r') as fulldata:   #Read from file
    lines = csv.reader(fulldata,delimiter=',',skipinitialspace=True)
    next(lines)
    for ele in lines:
        if ele[0] not in data:
            data[ele[0]] = {}
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        elif (ele[0] in data) and (ele[1] not in data[ele[0]]):
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        else:
            data[ele[0]][ele[1]] += [ele[2::]]

def days_data(data, Fam=0,c62_t=97,cr7_t=90,kg1_t=80,day=8):
    time = 24.0*day
    res = {}
    for i in range(Fam):
        fam = str(i)
        tot = 0; c62p = 0; cr7p = 0; kg1p = 0; 
        TCM = 0; TEM = 0; TF = 0
        for gen in data[fam]:
            for cell in range(len(data[fam][gen])):
                if (float(data[fam][gen][cell][6]) > time) and (float(data[fam][gen][cell][4]) > time) and (float(data[fam][gen][cell][2]) < time):
                    tot += 1
                    if float(data[fam][gen][cell][7])>c62_t: c62p+=1
                    if float(data[fam][gen][cell][8])>cr7_t: cr7p+=1
                    if float(data[fam][gen][cell][9])>kg1_t: kg1p+=1
                    if float(data[fam][gen][cell][7])>c62_t and float(data[fam][gen][cell][8])>cr7_t: TCM+=1
                    elif float(data[fam][gen][cell][7])<c62_t and float(data[fam][gen][cell][8])>cr7_t: TEM+=1
                    else: TF+=1
        if tot>0:
            res[fam] = {'TCM':TCM*1.0/tot, 'TEM':TEM*1.0/tot, 'TF':TF*1.0/tot, 'Size':tot}
        else: res[fam] = {'Size':0}
    return res

bp_data={}
summary = {}
TCM_m=[];TEM_m=[];TF_m=[]
TCM_sd=[];TEM_sd=[];TF_sd=[]

for t in range(1,9):
    TCM = []; TEM = []; TF = []
    bp_data["day "+str(t)]=days_data(data,Fam=val.Fam,day=t)
    for i in bp_data["day "+str(t)].keys():
        if bp_data["day "+str(t)][i]['Size'] > 0:
            TCM+=[bp_data["day "+str(t)][i]['TCM']]
            TEM+=[bp_data["day "+str(t)][i]['TEM']]
            TF+=[bp_data["day "+str(t)][i]['TF']]
    TCM_m += [mean(TCM)]; TEM_m += [mean(TEM)]; TF_m += [mean(TF)]
    TCM_sd += [std(TCM)]; TEM_sd += [std(TEM)]; TF_sd += [std(TF)]
    print "day "+str(t), " ", TCM_m[t-1], " ", TEM_m[t-1], " ", TF_m[t-1]

TCM_m.insert(0,1.0); TCM_sd.insert(0,0.0)
TEM_m.insert(0,0.0); TEM_sd.insert(0,0.0)
TF_m.insert(0,0.0); TF_sd.insert(0,0.0)

plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plt.plot(range(9),TCM_m,label='TCM',color='r',marker='o',linestyle='--',linewidth=2)
plt.plot(range(9),TEM_m,label='TEM',color='k',marker='s',linestyle='--',linewidth=2)
plt.plot(range(9),TF_m,label='TF',color='b',marker='^',linestyle='--',linewidth=2)
plt.xlabel('Days',fontsize=18)
plt.ylabel('Relative Subset Size',fontsize=18)
plt.savefig('TimePlot.svg',format='svg')
