# -*- coding: utf-8 -*-
"""
Created on Sun Apr 05 10:56:59 2015
To extract temporal data
@author: Aridaman
2 index of list is Birth,
4 index of list is Global TDivide,
6 index of list is Global TDie,
7 index of list is CD62L,
8 index of list is CCR7,
9 index of list is KLRG1,
"""
import csv, matplotlib, argparse
matplotlib.use('Agg')
import numpy as np
import matplotlib.pylab as plt
from matplotlib import rcParams

parser = argparse.ArgumentParser()
parser.add_argument('-fn', dest='fn', default="BP_CD8_4.5_1.25_0.2_1000_20_10.csv", type=str, help='File Name')
parser.add_argument('-Fam', dest='Fam', default=1000, type=int, help='Number of families')
val = parser.parse_args()

data = {}
c62_t=97; cr7_t=90; kg1_t=80

with open(val.fn,'r') as fulldata:   #Read from file
    lines = csv.reader(fulldata,delimiter=',',skipinitialspace=True)
    next(lines)
    for ele in lines:
        if ele[0] not in data:
            data[ele[0]] = {}
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        elif (ele[0] in data) and (ele[1] not in data[ele[0]]):
            data[ele[0]][ele[1]] = {}
            data[ele[0]][ele[1]] = [ele[2::]]
        else:
            #print ele[0], ele[1], ele[2::]
            data[ele[0]][ele[1]] += [ele[2::]]

def marker_data(data, Fam=0,c62_t=97,cr7_t=90,kg1_t=80,day=8):
    time = 24.0*day
    res = {'cd62':[], 'cr7':[], 'kg1':[]} # Create an empty dictionary to store normalized marker values
    for i in range(Fam):
        fam = str(i)
        tot = 0; #c62p = 0; cr7p = 0; kg1p = 0;
        for gen in data[fam]:
            for cell in range(len(data[fam][gen])):
                #print data[fam][gen][cell]
                if (float(data[fam][gen][cell][6]) > time) and (float(data[fam][gen][cell][4]) > time) and (float(data[fam][gen][cell][2]) < time):
                    tot += 1
                    res['cd62'] += [float(data[fam][gen][cell][7])/c62_t] # Normalize wrt threshold
                    res['cr7'] += [float(data[fam][gen][cell][8])/cr7_t] # Normalize wrt threshold
                    res['kg1'] += [float(data[fam][gen][cell][9])/kg1_t] # Normalize wrt threshold
    return res

bp_data={} # initialize the marker/day object

for t in range(1,9): # To get all marker data per day
    bp_data["day "+str(t)] = marker_data(data,Fam=val.Fam,day=t)
    print len(bp_data)


## Plot the data
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['kg1'],bp_data['day 2']['kg1'],bp_data['day 3']['kg1'],bp_data['day 4']['kg1'],bp_data['day 5']['kg1'],bp_data['day 6']['kg1'],bp_data['day 7']['kg1'],bp_data['day 8']['kg1']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Marker_kg1_daydist.svg',format='svg')

plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['cd62'],bp_data['day 2']['cd62'],bp_data['day 3']['cd62'],bp_data['day 4']['cd62'],bp_data['day 5']['cd62'],bp_data['day 6']['cd62'],bp_data['day 7']['cd62'],bp_data['day 8']['cd62']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Marker_cd62_daydist.svg',format='svg')

plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()
plot_data = [bp_data['day 1']['cr7'],bp_data['day 2']['cr7'],bp_data['day 3']['cr7'],bp_data['day 4']['cr7'],bp_data['day 5']['cr7'],bp_data['day 6']['cr7'],bp_data['day 7']['cr7'],bp_data['day 8']['cr7']]
parts = plt.violinplot(plot_data, range(1,9,1), points=20, widths=0.9, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,8,1):
    print i
    temp = np.percentile(plot_data[i], [25, 50, 75])
    print temp
    quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.savefig('Marker_cr7_daydist.svg',format='svg')



############ Plotting the marker distribution per generation ############
def marker_generation(data, Fam=0,c62_t=97,cr7_t=90,kg1_t=80, gen=1, day=1):
    #time = 24.0*day
    gen = str(gen)
    print gen, Fam#, time
    res = {'cd62':[], 'cr7':[], 'kg1':[]} # Create an empty dictionary to store normalized marker values
    for i in range(Fam):
        fam = str(i)
        tot = 0; #c62p = 0; cr7p = 0; kg1p = 0;
        #print len(data[fam][gen])
        try:
            for cell in range(len(data[fam][gen])):
                #print len(data[fam][gen][cell])
                #if (float(data[fam][gen][cell][6]) > time) and (float(data[fam][gen][cell][4]) > time) and (float(data[fam][gen][cell][2]) < time):
                tot += 1
                res['cd62'] += [float(data[fam][gen][cell][7])/c62_t] # Normalize wrt threshold
                res['cr7'] += [float(data[fam][gen][cell][8])/cr7_t] # Normalize wrt threshold
                res['kg1'] += [float(data[fam][gen][cell][9])/kg1_t] # Normalize wrt threshold'''
        except:
            print "Does not exist!"            
    return res


gen_cd62=[]
gen_cr7=[]
gen_kg1=[]

for gen in range(1,21): # To get all marker data per day
    print "gen = ", gen
    temp = marker_generation(data, Fam=val.Fam,day=2,gen=gen)
    #print temp
    gen_cd62 += [temp['cd62']]; gen_cr7 += [temp['cr7']]; gen_kg1 += [temp['kg1']]

## Plot the data
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()

len(gen_cd62)
parts = plt.violinplot(gen_cd62, range(1,21,1), points=20, widths=0.8, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,20,1):
    print i
    try:
        temp = np.percentile(gen_cd62[i], [25, 50, 75])
        quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
    except:
        print "Does not exist!"

print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.xticks(np.arange(1, 21, 1))
plt.savefig('Marker_cd62_gendist.svg',format='svg')

## Plot the data
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()

len(gen_cr7)
parts = plt.violinplot(gen_cr7, range(1,21,1), points=20, widths=0.8, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,20,1):
    print i
    try:
        temp = np.percentile(gen_cr7[i], [25, 50, 75])
        quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
    except:
        print "Does not exist!"

print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.xticks(np.arange(1, 21, 1))
plt.savefig('Marker_cr7_gendist.svg',format='svg')

## Plot the data
plt.ioff()
rcParams.update({'figure.autolayout': True})
plt.figure()

len(gen_kg1)
parts = plt.violinplot(gen_kg1, range(1,21,1), points=20, widths=0.8, 
                       showmeans=False, showextrema=False, showmedians=False)
for pc in parts['bodies']: # To change the colors of the violin plot
    pc.set_facecolor('#D43F3A')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

quartile1=[]; medians=[]; quartile3=[] # To calculate the quartiles
for i in range(0,20,1):
    print i
    try:
        temp = np.percentile(gen_kg1[i], [25, 50, 75])
        quartile1+=[temp[0]]; medians+=[temp[1]]; quartile3+=[temp[2]]
    except:
        print "Does not exist!"

print quartile1, medians, quartile3
inds = np.arange(1, len(medians) + 1)
plt.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
plt.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
plt.xticks(np.arange(1, 21, 1))
plt.savefig('Marker_kg1_gendist.svg',format='svg')
